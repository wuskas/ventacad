from sqlalchemy.ext.hybrid import hybrid_property
from ..database import db
from ..bcrypt import bcrypt




class User(db.Model):
    id=db.Column(db.Integer, primary_key = True)
    username=db.Column(db.String(length = 255), unique = True)
    email=db.Column(db.String(length = 255), unique = True)
    first_name=db.Column(db.String(length = 255))
    second_name=db.Column(db.String(length = 255))
    location=db.Column(db.String(length = 255))
    office=db.Column(db.String(length = 255))
    _password=db.Column('password', db.String(255))

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password=bcrypt.generate_password_hash(password).decode("utf-8")

    def __repr__(self):
        return '<User %r>' % self.username

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)
