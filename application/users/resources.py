from flask_restful import Resource, abort, reqparse, url_for, marshal, fields
from flask import g, request, jsonify, json
from flask_login import current_user, login_user, logout_user
from application import bcrypt


from application import db
from .models import User


class UserCurrent(Resource):
    def get(self):
        if not current_user.is_authenticated:
            return {'message': 'user is not authenticated'}, 401
        return {
            'id': current_user.id,
            'username': current_user.username,
            'email': current_user.email,
            'location': current_user.location,
            'office': current_user.office,
            'first_name': current_user.first_name,
            'second_name': current_user.second_name
        }, 200


class UserLogout(Resource):
    def post(self):
        logout_user()
        return {'message': 'successfully logout'}, 200


class UserLogin(Resource):
    def post(self):
        user_fields = {
            'id': fields.Integer,
            'username' : fields.String,
            'email': fields.String,
            'location': fields.String,
            'office': fields.String,
            'first_name': fields.String,
            'second_name': fields.String
        }
        username = request.get_json()['username']
        password = request.get_json()['password']
        user= User.query.filter_by(username = username).first()
        if not user:
            return {'message': 'Invalid username'}, 400
        if(not bcrypt.check_password_hash(user.password, password)):
            return {'message': 'Invalid password'}, 400
        login_user(user, remember=True)
        return marshal(current_user, user_fields), 200


class UserItem(Resource):
    def get(self, user_id):
        user = User.query.get(user_id)
        if not user:
            return {'message': 'not found'}, 400
        user_fields = {
            'id': fields.Integer,
            'username': fields.String,
            'email': fields.String,
            'location': fields.String,
            'office': fields.String,
            'first_name': fields.String,
            'second_name': fields.String
        }
        return marshal(list([user]), user_fields), 200

    def put(self, user_id):
        user = User.query.get(user_id)
        if not user:
            return {'message': 'we have not this user'}, 400
        user_fields = {
            'id': fields.Integer,
            'username': fields.String,
            'email': fields.String,
            'location': fields.String,
            'office': fields.String,
            'first_name': fields.String,
            'second_name': fields.String
        }
        data = request.json
        if not data:
            abort(400)
        for key, value in data.items():
            setattr(user, key, value)
        db.session.commit()
        return marshal(list([user]), user_fields), 200

    def delete(self, user_id):
        user = User.query.get(user_id)
        if not user:
            return {'message':'we have not this user'}, 400
        db.session.delete(user)
        db.session.commit()
        message = { 'message':'user is deleted'}
        return message, 200



class UserList(Resource):
    def get(self):
        user_fields = {
            'id': fields.Integer,
            'username' : fields.String,
            'email': fields.String,
            'location': fields.String,
            'office': fields.String,
            'first_name': fields.String,
            'second_name': fields.String
        }
        users = User.query.all()
        return marshal(list(users), user_fields), 200

    def post(self):
        data = request.json
        user = User(**data)
        db.session.add(user)
        try:
            db.session.commit()
        except sqlalchemy.ext.IntegrityError:
            abort(409, message = "User already exists!")
        return data, 201
