from flask import Flask, render_template, json, make_response


from .database import db
from .bcrypt import bcrypt
from .authorization import login_manager
from .api import api
from .users.models import User


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

def create_app(config = None):
    app = Flask(__name__)
    if config is not None:
        app.config.from_object(config)
    app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    db.init_app(app)
    bcrypt.init_app(app)
    app.static_folder='../static'
    login_manager.init_app(app)
    from .users.resources import UserList, UserItem, UserLogin, UserLogout, UserCurrent
    api.add_resource(UserItem, '/api/users/<int:user_id>')
    api.add_resource(UserLogin, '/api/users/login')
    api.add_resource(UserLogout, '/api/users/logout')
    api.add_resource(UserList, '/api/users')
    api.add_resource(UserCurrent, '/api/users/current')
    api.init_app(app)

    @app.route('/')
    def index():
        return render_template('index.jade')

    return app
