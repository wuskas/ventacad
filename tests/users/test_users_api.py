from application.users.models import User
from flask import json
from flask_login import login_user
import base64

def test_create_new_user(db, session, client):
    data = {
        'username': 'amir47',
        'email': 'clon45@mail.ru',
        'first_name': 'Амир',
        'second_name': 'Сайфутдинов',
        'location': 'Россия, Оренбург',
        'office': 'Инженер',
        'password': 'qwerty123456'
        }
    response = client.post('/api/users', data=json.dumps(data), content_type='application/json')
    assert response.status_code == 201

def test_get_users_list(db, session, client):
    data = {
        'username': 'amir474',
        'email': 'clon4543@mail.ru',
        'first_name': 'Амир',
        'second_name': 'Сайфутдинов',
        'location': 'Россия, Оренбург',
        'office': 'Инженер',
        'password': 'qwerty123456'
        }

    data1 = {
        'username': 'amir473',
        'email': 'clon453@mail.ru',
        'first_name': 'Амир',
        'second_name': 'Сайфутдинов',
        'location': 'Россия, Оренбург',
        'office': 'Инженер',
        'password': 'qwerty123456'
        }
    user = User(**data)
    user1 = User(**data1)
    session.add(user)
    session.add(user1)
    session.commit()
    response = client.get('/api/users')
    if response.json[0]['username'] == 'amir47':
        assert response.status_code == 200

def test_get_user(db, session, client):
    data = {
    'username': 'amir47423',
    'email': 'clon454323@mail.ru',
    'first_name': 'Амир',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер',
    'password': 'qwerty123456'
    }
    user = User(**data)
    session.add(user)
    session.commit()
    response = client.get('/api/users/{}'.format(user.id))
    assert response.status_code == 200

def test_update_user(db, session, client):
    data = {
    'username': 'amir47423343423',
    'email': 'clon45432334343423@mail.ru',
    'first_name': 'Амир',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер',
    'password': 'qwerty123456'
    }
    user = User(**data)
    session.add(user)
    session.commit()
    data_2 = {
    'username': 'damir',
    'email': 'damir@mail.ru',
    'first_name': 'Дамир',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер'
    }
    response = client.put('/api/users/{}'.format(user.id),data=json.dumps(data_2),
        content_type='application/json')
    assert response.status_code == 200
    assert response.json[0]['email'] == 'damir@mail.ru'

def test_delete_user(db, session, client):
    data = {
    'username': 'amir47423343423',
    'email': 'clon45432334343423@mail.ru',
    'first_name': 'Амир',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер',
    'password': 'qwerty123456'
    }
    user = User(**data)
    session.add(user)
    session.commit()
    response = client.delete('/api/users/{}'.format(user.id))
    assert response.status_code == 200

def test_login_user(db, session, client):
    data = {
    'username': '12121212',
    'email': '12121212@mail.ru',
    'first_name': 'Амир',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер',
    'password': 'qwerty123456'
    }
    user = User(**data)
    session.add(user)
    session.commit()
    data = {
    'username': '12121212',
    'password': 'qwerty123456'
    }
    response = client.post('/api/users/login', data=json.dumps(data), content_type='application/json')
    assert response.status_code == 200

def test_login_user(db, session, client):
    data = {
    'username': '12121212',
    'email': '12121212@mail.ru',
    'first_name': 'Амир',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер',
    'password': 'qwerty123456'
    }
    user = User(**data)
    session.add(user)
    session.commit()
    data = {
    'username': '12121212',
    'password': 'qwerty123456'
    }
    response = client.post('/api/users/login', data=json.dumps(data), content_type='application/json')
    assert response.status_code == 200



def test_logout_user(db, session, client):
    data = {
    'username': 'vasya',
    'email': 'vasya@mail.ru',
    'first_name': 'Вася',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер',
    'password': 'qwerty123456'
    }
    user = User(**data)
    session.add(user)
    session.commit()
    response = client.post('/api/users/login', data=json.dumps(data), content_type='application/json')
    response = client.post('/api/users/logout')
    assert response.status_code == 200

def test_current_user(db, session, client):
    data = {
    'username': 'vasya1',
    'email': 'vasya1@mail.ru',
    'first_name': 'Вася',
    'second_name': 'Сайфутдинов',
    'location': 'Россия, Оренбург',
    'office': 'Инженер',
    'password': 'qwerty123456'
    }
    user = User(**data)
    session.add(user)
    session.commit()
    response = client.post('/api/users/login', data=json.dumps(data), content_type='application/json')
    response = client.get('/api/users/current')
    assert response.status_code == 200
