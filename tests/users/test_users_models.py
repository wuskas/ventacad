from application.users.models import User

def test_create_user_instance(session):

    user_data = {
        'username'    : 'amir',
        'email'       : 'clon245@mail.ru',
        'first_name'  : 'Амир',
        'second_name' : 'Сайфутдинов',
        'location'    : 'Россия, Оренбург',
        'office'      : 'Инженер',
        '_password'    : 'qwerty123456'
    }

    user = User(**user_data)

    session.add(user)
    session.commit()

    assert user.id is not None
