MessageComplete = require('./MessageComplete');


var data = {
  'username'    : null,
  'email'       : null,
  'first_name'  : null,
  'second_name' : null,
  'location'    : null,
  'office'      : null,
  'password'    : null
}

var RegistrationForm = React.createClass({

    handlePasswordChange: function(event) {
      data.password =  event.target.value
    },
    handleOfficeChange: function(event) {
      data.office =  event.target.value
    },
    handleLocationChange: function(event) {
      data.location =  event.target.value
    },
    handleSecondNameChange: function(event) {
      data.second_name =  event.target.value
    },
    handleFirstNameChange: function(event) {
      data.first_name =  event.target.value
    },
    handleUserNameChange: function(event) {
      data.username =  event.target.value
    },
    handleEmailChange: function(event) {
      data.email =  event.target.value
    },
    regСompleted: function(){
      $.ajax({
        url: '/api/users',
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function(response) {
        },
        error: function(response){
        }
      });
      ReactDOM.render(
        <MessageComplete data = {data}/>,
        document.getElementById('content')
      );
    },

    render: function() {
      return (<div className ="container">
                <form method="post" id="registration" ><h1>Registration form</h1>
                  <div className = "row">
                    <div className = "six columns">
                      <label>Логин</label>
                      <input type = "text" name = "username" className = "u-full-width"
                      onChange={this.handleUserNameChange} />
                      <label>Электронная почта</label>
                      <input type = "text" name = "email" className = "u-full-width"
                        onChange={this.handleEmailChange} />
                      <label>Пароль</label>
                      <input type = "password" name = "password" className = "u-full-width"
                        onChange={this.handlePasswordChange}/>
                      <label>Имя</label>
                      <input type = "text" name = "first_name" className = "u-full-width"
                        onChange={this.handleFirstNameChange}/>
                    </div>
                    <div className = "six columns">
                      <label>Фамилия</label>
                      <input type = "text" name = "second_name" className = "u-full-width"
                        onChange={this.handleSecondNameChange}/>
                      <label>Страна, город</label>
                      <input type = "text" name = "location" className = "u-full-width"
                        onChange={this.handleLocationChange}/>
                      <label>Должность</label>
                      <select name = "office" className = "u-full-width"
                        onChange={this.handleOfficeChange}>
                        <option value="Инженер">Инженер</option>
                        <option value="Преподаватель">Преподаватель</option>
                        <option value="Студент">Студент</option>
                      </select>
                      <a className = "button button-primary submit-button u-full-width"
                        onClick={this.regСompleted}>Завершить регистрацию!</a>
                    </div>
                  </div>
                </form>
              </div>)
  }
});

module.exports = RegistrationForm;
