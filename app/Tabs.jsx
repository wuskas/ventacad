LoginForm = require('./LoginForm');
RegistrationForm = require('./RegistrationForm');


var Tabs = React.createClass({

  toggleTab: function(tab) {
    this.props.toggleTab(tab)
  },
  render: function() {

    return (
    <div>
      <div className ="container">
        <a className = "button button-primary submit-button u-full-width"
          onClick={this.toggleTab.bind(this, 'loginForm')}>login form</a>
        <a className = "button button-primary submit-button u-full-width"
          onClick={this.toggleTab.bind(this, 'registrationForm')}>registration form</a>
      </div>
      <div>{this.props.proc.tabStatus == 'loginForm' ?
        <LoginForm statusForm = {this.props.proc.statusForm} toggleStatusForm = {this.props.toggleStatusForm}/>
          : <RegistrationForm />}
      </div>
    </div>);
  }
});
module.exports =  Tabs;
