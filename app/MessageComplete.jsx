

var MessageComplete = React.createClass({
  render: function(){
    return <div>{this.props.formStatus == 'logged' ? <MessageLogin /> : <MessageComplete />}</div>;
  }
});

var MessageLogin = React.createClass({

  render: function(){
    return <h1>Успешно авторизованы!</h1>
  }
});

var MessageRegister = React.createClass({
  render: function(){
    return <h1>Регистрация прошла успешно!</h1>
  }
});


module.exports = MessageComplete;
