MessageComplete = require('./MessageComplete');


var login_data = {
  'username': null,
  'password': null
}

var LoginForm = React.createClass({
  login: function(){
    $.ajax({
      url: '/api/users/login',
      type: 'POST',
      dataType: "json",
      contentType: 'application/json',
      data: JSON.stringify(login_data),
      success: function(response) {
      },
      error: function(response){
      }
    });
      this.props.toggleStatusForm('logged');

      ReactDOM.render(
        <MessageComplete />,
        document.getElementById('content')
      );
  },
  handlePasswordChange: function(event) {
    login_data['password'] = event.target.value
  },
  handleUserNameChange: function(event) {
    login_data['username'] = event.target.value
  },
  render: function(){
    return (<div className ="container">
              <form id="loginForm"><h1>Login form</h1>
                <div className = "row">
                <div className = "six columns">
                    <label>Логин</label>
                    <input type = "text" name = "username" className = "u-full-width"
                      onChange={this.handleUserNameChange} />
                    <label>Пароль</label>
                    <input type = "password" name = "password" className = "u-full-width"
                      onChange={this.handlePasswordChange}/>
                    <a className = "button button-primary submit-button u-full-width"
                      onClick={this.login}>Войти!</a>
                  </div>
                </div>
              </form>
            </div>)
  }
});

module.exports = LoginForm;
