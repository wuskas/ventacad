Tabs = require('./Tabs')

var App = React.createClass({
  getInitialState: function () {
    return {
      tabStatus:'loginForm',
      statusForm: 'not complited'
    }
  },
  toggleTab: function(tab) {
    this.setState({tabStatus: tab});
  },
  toggleStatusForm: function(status){
    this.setState({statusForm: status});
  },
  render: function (){
    return (<Tabs proc = {this.state} toggleTab = {this.toggleTab}
      toggleStatusForm = {this.toggleStatusForm}/>);
  }
});



ReactDOM.render(
  <App  />,
  document.getElementById('content')
);

module.exports = App;
