from flask_script import Manager
from application import create_app, db

manager = Manager(app=create_app('settings'))

@manager.command
def init_db():
    db.create_all()

if __name__ == '__main__':
    manager.run()